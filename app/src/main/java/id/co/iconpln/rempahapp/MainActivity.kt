package id.co.iconpln.rempahapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.core.view.get
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.item_list_rempah.view.*
import kotlin.system.exitProcess

class MainActivity : AppCompatActivity() {

    private var doubleBackToExitPressedOnce = false

    private val listRempah: ListView
        get() = lvListRempah

    private val list: ArrayList<Rempah> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadListArrayAdapter()
        loadListBaseAdapter(this)
        setItemOnClickListener(listRempah)

        actionTitleBar()

    }

    private fun actionTitleBar() {
        val actionBar = supportActionBar
            actionBar?.title = "Rempah-rempah"
    }

    private fun loadListBaseAdapter(context: Context){
        list.addAll(RempahsData.listDataRempah)

        val baseAdapter = ListViewRempahAdapter(context, list)
        listRempah.adapter = baseAdapter
    }

    private fun loadListArrayAdapter(){
        val adapter =ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getDataRempah())
        listRempah.adapter = adapter
    }

    private fun getDataRempah(): Array<String>{
        val rempah = arrayOf("Jahe", "Kencur", "Kunyit", "Temulawak", "Merica")
        return rempah
    }

    //add toast
    private fun setItemOnClickListener(listView: ListView){
        listView.setOnItemClickListener { parent, view, index, id ->
            Toast.makeText(this,list[index].name, Toast.LENGTH_SHORT).show()

            showDetailRempah(list[index])
        }
    }

    private fun showDetailRempah(rempah: Rempah) {
        val detailRempahIntent = Intent(this, DetailRempahActivity::class.java)
        detailRempahIntent.putExtra(DetailRempahActivity.EXTRA_REMPAH, rempah)

        startActivity(detailRempahIntent)
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce){
            super.onBackPressed()
            moveTaskToBack(true)
            exitProcess(-1)
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT  ).show()
        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }
}
