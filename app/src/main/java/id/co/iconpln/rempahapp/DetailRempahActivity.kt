package id.co.iconpln.rempahapp

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.AlertDialogLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_detail_rempah.*
import kotlin.system.exitProcess

class DetailRempahActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_NAME = "extra_name"
        const val EXTRA_DESC = "extra_desc"
        const val EXTRA_IMAGE_URL = "extra_image_url"
        const val EXTRA_SOURCE = "extra_source"

        const val EXTRA_REMPAH = "extra_rempah"
    }

    private lateinit var rempah: Rempah

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_rempah)

        setupActionBar()
        initIntentExtra()
        displayRempahDetail()

        shareSiteOnClickListener()

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when {
            item.itemId == R.id.itemShare -> {
                val shareData = this.rempah.name + "\n" +
                        this.rempah.desc + "\n" +
                        this.rempah.source

                val shareDataIntent = Intent(Intent.ACTION_SEND)
                shareDataIntent.putExtra(Intent.EXTRA_TEXT, shareData)
                shareDataIntent.type = "text/plain"

                val shareIntent = Intent.createChooser(shareDataIntent, "share using ..")
                startActivity(shareIntent)

            }
            item.itemId == R.id.itemShareLink -> {
                val shareLinkData = this.rempah.source
                val shareDataLinkIntent = Intent(Intent.ACTION_SEND)
                shareDataLinkIntent.putExtra(Intent.EXTRA_TEXT, shareLinkData)
                shareDataLinkIntent.type = "text/plain"

                val shareIntent = Intent.createChooser(shareDataLinkIntent, "share link via ..")
                startActivity(shareIntent)
            }
            item.itemId == R.id.itemExit -> {
                moveTaskToBack(true)
                exitProcess(-1)
            }
        }
        //back action
        return when (item.itemId){
            android.R.id.home -> {
                finish()
                true
            }else ->{
                false
            }
        }
    }

    private fun shareSiteOnClickListener() {
        btnDetailSource.setOnClickListener {
            val shareSite = Uri.parse(this.rempah.source)
            val openWebIntent = Intent(Intent.ACTION_VIEW, shareSite)

            startActivity(openWebIntent)
        }
    }



    private fun setupActionBar() {
        supportActionBar?.title = "Detail Hero"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initIntentExtra() {
        rempah = intent.getParcelableExtra(EXTRA_REMPAH)
    }

    private fun displayRempahDetail() {
        tvDetailTitleRempah.text = rempah.name
        tvDetailRempahDesc.text = rempah.desc
        tvSourceDetail.text = rempah.source
        Glide.with(this)
            .load(rempah.photo)
            .apply {
                (
                        RequestOptions()
                            .centerInside()
                            .placeholder(R.drawable.ic_launcher_background)
                            .error(R.drawable.ic_launcher_foreground)
                        )

            }
            .into(ivDetailPhotoRempah)
    }

}
