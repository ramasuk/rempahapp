package id.co.iconpln.rempahapp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Rempah(
    var name: String = "",
    var desc: String = "",
    var photo: String = "",
    var source: String =""
) : Parcelable