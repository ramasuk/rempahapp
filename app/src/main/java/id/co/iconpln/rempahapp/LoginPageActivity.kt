package id.co.iconpln.rempahapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Patterns
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login_page.*

class LoginPageActivity : AppCompatActivity(), View.OnClickListener {

    private var doubleBackToExit = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_page)

        btnLogin.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.btnLogin -> checkLogin()
        }
    }

    private fun checkLogin(){
        val email = etLoginEmail.text.toString()
        val password = etLoginPassword.text.toString()

        when{
            etLoginEmail.text.isBlank() -> etLoginEmail.error = "can't be blank"
            etLoginPassword.text.isBlank() -> etLoginPassword.error = "can't be blank"
            etLoginPassword.text.length < 7 -> etLoginPassword.error = "password must be 7 characters"
            !Patterns.EMAIL_ADDRESS.matcher(etLoginEmail.text).matches() -> etLoginEmail.error = "wrong email format"
            else -> {
                if (email == "user@mail.com" && password == "password"){
                    val mainActivityIntent = Intent(this, MainActivity::class.java)
                    startActivity(mainActivityIntent)
                    Toast.makeText(this, "log in",Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this,"wrong email or password",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onBackPressed() {
        if(doubleBackToExit){
            super.onBackPressed()
            return
        }
        this.doubleBackToExit = true
        Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT  ).show()
        Handler().postDelayed(Runnable { doubleBackToExit = false }, 2000)

    }

}
